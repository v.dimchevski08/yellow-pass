// DOM Elements
const resultDisplay = document.getElementById("result");
const passLength = document.getElementById("length");
const uppercase = document.getElementById("uppercase");
const lowercase = document.getElementById("lowercase");
const number = document.getElementById("numbers");
const symbol = document.getElementById("symbols");
const generateBtn = document.getElementById("generate");
const clipboardBtn = document.getElementById("clipboard");

const randomFunc = {
  upper: getRandomUpper,
  lower: getRandomLower,
  number: getRandomNumber,
  symbol: getRandomSymbol,
};

// Generate button event listener
generateBtn.addEventListener("click", () => {
  const length = +passLength.value;
  const hasUpper = uppercase.checked;
  const hasLower = lowercase.checked;
  const hasNumber = number.checked;
  const hasSymbol = symbol.checked;

  resultDisplay.innerText = generatePass(
    length,
    hasUpper,
    hasLower,
    hasNumber,
    hasSymbol
  );
});

// Copy to clipboard
clipboardBtn.addEventListener("click", () => {
  const textarea = document.createElement("textarea");
  const password = resultDisplay.innerText;

  if (!password) {
    return;
  }

  textarea.value = password;
  document.body.appendChild(textarea);
  textarea.select();
  document.execCommand("copy");
  textarea.remove();
  alert("Password copied to clipboard.");
});

// Generate password function
function generatePass(length, upper, lower, number, symbol) {
  let generatedPass = "";
  const optionsCount = upper + lower + number + symbol;
  const optionsArr = [{ upper }, { lower }, { number }, { symbol }].filter(
    (option) => Object.values(option)[0]
  );

  if (optionsCount === 0) {
    return "";
  }

  for (let i = 0; i < length; i += optionsCount) {
    optionsArr.forEach((option) => {
      const funcName = Object.keys(option)[0];
      generatedPass += randomFunc[funcName]();
    });
  }

  const finalPass = generatedPass.slice(0, length);
  return finalPass;
}

// Generator functions
function getRandomUpper() {
  return String.fromCharCode(Math.floor(Math.random() * 26) + 65);
}

function getRandomLower() {
  return String.fromCharCode(Math.floor(Math.random() * 26) + 97);
}

function getRandomNumber() {
  return String.fromCharCode(Math.floor(Math.random() * 10) + 48);
}

function getRandomSymbol() {
  const symbols = "!@#$%^&*(){}[]=<>/,.";
  return symbols[Math.floor(Math.random() * symbols.length)];
}
